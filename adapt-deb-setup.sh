#!/bin/bash

################################################################
#
#		Title					adapt-deb-setup.sh
#		Description		Adapt setup script for Debian or Ubuntu
#		Author				Sam Howell
#		Email					mail@samhowell.uk
#
################################################################

green='\e[0;32m'
yellow='\e[0;33m'
red='\e[0;31m'
reset='\e[0m'
tick=$green['\xE2\x9C\x94']$reset
supported_versions=("buster" "stretch" "xenial" "bionic" "focal")

function print_success () {
	printf "$tick\n"
}

function check_result () {
	if [ $? -ne 0 ]; then
		echo -e $red"[!] Something went wrong. Check $log before trying again."$reset
		sleep 1
		exit 1
	fi
}

if [[ $(uname -s) != "Linux" ]]; then
	echo -e $red"\n[!] You must be using a GNU/Linux distribution to use this script."$reset
	exit 1
else
	version=$(cat /etc/*-release | grep "VERSION_CODENAME" | tr -d 'VERSION_CODENAME=')
	if [[ ! ${supported_versions[*]} =~ $version ]]; then
		echo -e $red"[!] Your distribution version ($version) is not supported by a dependency (MongoDB)."$reset
		exit 1
	fi

	echo -e "\nAdapt setup for $version\n"
	sleep 1
	echo "[i] The Adapt authoring tool, CLI and dependencies will now be installed."
	echo "[i] This will require sudo privileges, so you may be asked to enter your password."
	echo -n -e $yellow"[i] Would you like to continue? [y/N] "$reset
	read answer

	if [[ $answer == "y" ]] || [[ $answer == "Y" ]]; then
		ls $HOME/adapt > /dev/null 2>&1
		if [[ $? -eq 0 ]]; then
			echo -e $yellow"[i] Directory already exists. Not overwriting."$reset
			echo -n -e $yellow"[i] Enter a name for the new Adapt directory: "$reset
			read name
			while [[ $name == "adapt" ]]; do
				echo -n -e $yellow"[i] Enter a different name for the new Adapt directory: "$reset
				read name
			done
			mkdir $HOME/$name
		fi

		path=$HOME/$name
		touch $path/adapt-install.log
		log=$path/adapt-install.log
    if [[ $(pwd) != $HOME ]]; then
      echo -n -e "\n[i] Moving to $path... "
      cd $path
		  print_success
    fi

		echo -n "[i] Upgrading apt packages... "
		sudo apt-get -y update > /dev/null 2>&1
		sudo apt-get -y upgrade > /dev/null 2>&1
		check_result
		print_success

		command -v wget > /dev/null 2>&1
		if [[ $? -ne 0 ]]; then
			echo -n "[i] Installing wget... "
			sudo apt-get -y install wget >> $log 2>&1
			check_result
			print_success
		fi

		command -v git > /dev/null 2>&1
		if [[ $? -ne 0 ]]; then
			echo -n "[i] Installing git... "
			sudo apt-get -y install git >> $log 2>&1
			check_result
			print_success
		fi

		command -v curl > /dev/null 2>&1
		if [[ $? -ne 0 ]]; then
			echo -n "[i] Installing curl... "
			sudo apt-get -y install curl >> $log 2>&1
			check_result
			print_success
		fi

		command -v nvm > /dev/null 2>&1
		if [[ $? -ne 0 ]]; then
			echo -n "[i] Installing NVM... "
      curl -s -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash >> $log 2>&1
      export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
			check_result
			print_success
		fi

		command -v node > /dev/null 2>&1
		if [[ $? -ne 0 ]]; then
			echo -n "[i] Installing Node.js LTS... "
			nvm install --lts >> $log 2>&1
			check_result
			print_success
		fi

		command -v grunt-cli > /dev/null 2>&1
		if [[ $? -ne 0 ]]; then
			echo -n "[i] Installing Grunt... "
			npm install -g grunt-cli >> $log 2>&1
			check_result
			print_success
		fi

		command -v gpg > /dev/null 2>&1
		if [[ $? -ne 0 ]]; then
			echo -n "[i] Installing GnuPG... "
			sudo apt-get -y install gpg >> $log 2>&1
			check_result
			print_success
		fi

		echo -n "[i] Installing MongoDB... "
    # We're using v4.x due to specific hardware requirements for v5.x
    # See https://jira.mongodb.org/browse/SERVER-59482?focusedCommentId=4015942&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-4015942
		wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add - >> $log 2>&1
		sudo touch /etc/apt/sources.list.d/mongodb-org-4.4.list

		if [[ $version == "buster" ]] || [[ $version == "stretch" ]]; then
			echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/debian $version/mongodb-org/4.4 main" | sudo tee -a /etc/apt/sources.list.d/mongodb-org-4.4.list > /dev/null 2>&1
			check_result
		else
			echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu $version/mongodb-org/4.4 multiverse" | sudo tee -a /etc/apt/sources.list.d/mongodb-org-4.4.list > /dev/null 2>&1
			check_result
		fi
		sudo apt-get -y update >> $log 2>&1
		check_result
		sudo apt-get -y install mongodb-org >> $log 2>&1
		check_result
		print_success

		echo -n "[i] Starting mongod service... "
		sudo systemctl start mongod >> $log 2>&1
		check_result
		print_success

		echo -n "[i] Installing Adapt CLI... "
		npm install -g adapt-cli >> $log 2>&1
		check_result
		print_success

		echo -n "[i] Cloning Adapt Authoring Tool... "
		git clone https://github.com/adaptlearning/adapt_authoring.git >> $log 2>&1
		check_result
		print_success
		cd adapt_authoring

		echo "[i] Installing and configuring Adapt Authoring Tool... "
		npm install --production >> $log 2>&1
		check_result
    source ~/.bashrc
		node install
		check_result

		echo -e "[i] Refer to '$log' for more information.\n"
		exit 0
	else
		echo -e "[i] Exiting...\n"
		exit 0
	fi
fi
