# adapt-deb-setup

Bash script for the installation and setup of the **[Adapt](https://github.com/adaptlearning) authoring tool, CLI and dependencies** for Debian and Ubuntu. These tools are required for creating, developing and publishing Adapt courses and plugins.

This is a simple script written only to make it easier and quicker for users to get up and running with Adapt. For users who want to learn more about the programs which are being installed, and why they're needed to use Adapt, you're encouraged to take some time to read the [Adapt wiki](https://github.com/adaptlearning/adapt_authoring/wiki) and complete each installation step manually.

Like Adapt itself, this is free open source software, so you're also encouraged to modify this script to suit your needs and make a merge request if you feel the community could benefit from your changes.

# Verify

Move to the directory with the downloaded files (e.g. Downloads):
```
cd /home/$USER/Downloads
```
Check the integrity of the script:
```
sha256sum -c adapt-deb-setup.sh.SHA256
```
You should see the following output:
```
adapt-deb-setup.sh: OK
```

# Use

Make the script executable:
```
sudo chmod +x adapt-deb-setup.sh
```
Execute the script:
```
./adapt-deb-setup.sh
```

# Limitations

This script can only be used with the Debian and Ubuntu GNU/Linux distributions, and by a user in the sudo group.
